import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from "../components/dashboard/Dashboard";
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Dashboard,
        children: [
            {
                path: 'about',
                name: 'О нас',
                component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
            },
            {
                path: 'Gaming',
                name: 'Игры',
                component: () => import(/* webpackChunkName: "about" */ '../views/gaming/GamingInfo')
            }
        ]
    }
]

const router = new VueRouter({
    routes
})

export default router
